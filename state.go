package main

import (
	"sync"
)

type StateManager struct {
	cache map[int64]string
	sync.RWMutex
}

type State struct {
	ChatID int64  `bson:"_id" json:"chatId"`
	State  string `bson:"state" json:"state"`
}

func NewStateManager() *StateManager {
	return &StateManager{
		cache: map[int64]string{},
	}
}

func (sm *StateManager) SetState(chatID int64, state string) {
	sm.Lock()
	sm.cache[chatID] = state
	sm.Unlock()
}

func (sm *StateManager) GetState(chatID int64) string {
	sm.Lock()
	defer sm.Unlock()
	if _, ex := sm.cache[chatID]; ex {
		return sm.cache[chatID]
	}
	return ""
}
