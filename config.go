package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
)

type Config struct {
	BotToken string `json:"botToken"`
	DBName   string `json:"dbName"`
	DBPort   string `json:"dbPort"`
}

func NewConfig(filepath string) (*Config, error) {
	content, err := ioutil.ReadFile(filepath)
	if err != nil {
		log.Printf("[CONFIG] Error occured while reading config file: %s\n", err.Error())
		return &Config{}, err
	}
	cfg := &Config{}
	if err := json.Unmarshal(content, cfg); err != nil {
		log.Printf("[CONFIG] Error occured while json decoding: %s\n", err.Error())
		return &Config{}, err
	}

	return cfg, nil
}
