package main

import (
	"os"

	mgo "gopkg.in/mgo.v2"
)

type obj map[string]interface{}

var (
	db *mgo.Database
)

func main() {
	// Init config data
	config, err := NewConfig("config.json")
	if err != nil {
		os.Exit(1)
	}

	// Database Initialization
	session, err := mgo.Dial(config.DBPort)
	if err != nil {
		panic(err)
	}
	defer session.Close()
	db = session.DB(config.DBName)

	// Bot Initialization
	telegramBot, err := NewTelegramBot(config.BotToken)
	if err != nil {
		os.Exit(1)
	}
	go telegramBot.playlistManager.UpdateCache()

	if err := telegramBot.Polling(); err != nil {
		os.Exit(1)
	}
}
