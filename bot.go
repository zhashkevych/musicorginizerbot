package main

import (
	"fmt"
	"log"
	"net/url"
	"os"
	"strings"

	"github.com/Syfaro/telegram-bot-api"
	"gopkg.in/night-codes/types.v1"
)

// TelegramBot includes Syfaro's BotAPI and custom fields
type TelegramBot struct {
	bot             *tgbotapi.BotAPI
	stateManager    *StateManager
	playlistManager *PlaylistManager
}

// NewTelegramBot creates new instance of TelegramBot
func NewTelegramBot(token string) (*TelegramBot, error) {
	bot, err := tgbotapi.NewBotAPI(token)
	if err != nil {
		log.Printf("[ERROR] Create bot : %s\n", err.Error())
		return &TelegramBot{}, err
	}

	return &TelegramBot{
		bot:             bot,
		stateManager:    NewStateManager(),
		playlistManager: NewPlaylistManager(),
	}, nil
}

func (tb *TelegramBot) Polling() error {
	tb.bot.Debug = true
	log.Printf("Authorized on account %s\n", tb.bot.Self.UserName)

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates, err := tb.bot.GetUpdatesChan(u)
	if err != nil {
		log.Printf("[ERROR] failed recieving update channel: %s\n", err)
		return err
	}

	for update := range updates {
		if tb.CallbackHandler(update) {
			continue
		}
		if tb.StateHandler(update) {
			continue
		}

		if update.Message != nil && update.Message.IsCommand() {
			if update.Message.Command() == "start" {
				tb.stateManager.SetState(update.Message.Chat.ID, MainState)
			}
		}
	}
	return nil
}

// CallbackHandler checks for inline buttons actions
func (tb *TelegramBot) CallbackHandler(update tgbotapi.Update) bool {
	if callback := update.CallbackQuery; callback != nil {
		query := callback.Data
		// Handle button presses
		if query == CreatePlaylistButton {
			msg := tgbotapi.NewMessage(callback.Message.Chat.ID, "Введи название для плейлиста:")
			tb.stateManager.SetState(callback.Message.Chat.ID, SetPlaylistTitleState)
			go tb.bot.Send(msg)
			return true
		}
		if query == AddSongToPlaylistButton {
			playlists := tb.playlistManager.GetAllPlaylistsFromDB(callback.Message.Chat.ID)
			markup := AllPlaylistsKeyboard(playlists, "add")

			msg := tgbotapi.NewEditMessageText(callback.Message.Chat.ID, callback.Message.MessageID, "")
			msg.Text = "Выбери плейлист:"
			msg.ReplyMarkup = &markup
			tb.bot.Send(msg)
			return true
		}
		if query == RemovePlaylistButton {
			playlists := tb.playlistManager.GetAllPlaylistsFromDB(callback.Message.Chat.ID)
			markup := AllPlaylistsKeyboard(playlists, "remove")
			msg := tgbotapi.NewEditMessageText(
				callback.Message.Chat.ID,
				callback.Message.MessageID,
				"")
			msg.ParseMode = "markdown"
			msg.Text = "-\n-\n-\nВыбери плейлист который ты хочешь удалить\n-\n-\n-"
			msg.ReplyMarkup = &markup
			tb.bot.Send(msg)
			return true
		}
		if query == RemovePlaylistCancelButton {
			tb.stateManager.SetState(callback.Message.Chat.ID, MainState)
			tb.SendMainPanelEditMessage(callback.Message.Chat.ID, callback.Message.MessageID)
			return true
		}
		if query == AllPlaylistsButton {
			playlists := tb.playlistManager.GetAllPlaylistsFromDB(callback.Message.Chat.ID)
			markup := AllPlaylistsKeyboard(playlists, "")
			msg.Text = "Ваши плейлисты:"
			msg.ReplyMarkup = &markup
			tb.bot.Send(msg)
			return true
		}
		if query == ReturnButton {
			tb.SendMainPanelEditMessage(
				callback.Message.Chat.ID,
				callback.Message.MessageID,
			)
			tb.stateManager.SetState(callback.Message.Chat.ID, MainState)
			return true
		}
		if query == DownloadButton {
			if fileID, ok := tb.downloadVideo(message); ok {
				// Show playlist choice
				playlists := tb.playlistManager.GetAllPlaylistsFromDB(message.Chat.ID)

				msg := tgbotapi.NewMessage(message.Chat.ID, "Выбери плейлист, в который добавить только-что скаченную композицию")
				msg.ReplyMarkup = AllPlaylistsKeyboard(playlists, fmt.Sprintf("add:%s", fileID))
				tb.bot.Send(msg)
				return true
			}
		}
		// Select playlist
		if value, ok := tb.GetStateParams(callback.Data, SelectPlaylistButton); ok {
			switch value.(type) {
			case obj:
				if val, exists := value.(obj)["action"]; exists {
					if id, ex := value.(obj)["playlist"]; ex {
						// Add downloaded song to playlist
						if val == "add" {
							if fileID, e := value.(obj)["fileID"]; e {
								go tb.playlistManager.AddSongToPlaylist(types.Uint64(id), fileID.(string))
								tb.stateManager.SetState(callback.Message.Chat.ID, MainState)
								tb.SendMainPanelEditMessage(callback.Message.Chat.ID, callback.Message.MessageID)
								return false
							}
							// add songs to playlist button
							tb.stateManager.SetState(callback.Message.Chat.ID, AddSongToPlaylistState+":"+types.String(id))

							msg := tgbotapi.NewMessage(callback.Message.Chat.ID, "Теперь добавь песни в плейлист, когда закончишь, напиши /create.")
							tb.bot.Send(msg)
							return true
						}
						// Remove playlist
						if val == "remove" {
							tb.stateManager.SetState(callback.Message.Chat.ID, fmt.Sprintf("%s:%s", RemovePlaylistState, id))
							return false
						}
					}
				}
			case string:
				tb.stateManager.SetState(update.CallbackQuery.Message.Chat.ID, ActivePlaylistState+":"+value.(string))
				return false
			}
		}
		// Remove playlist confirm button pressed
		if value, ok := tb.GetStateParams(callback.Data, RemovePlaylistConfirmButton); ok {
			go tb.playlistManager.RemovePlaylist(types.Uint64(value.(string)))
			tb.stateManager.SetState(callback.Message.Chat.ID, MainState)
			tb.SendMainPanelEditMessage(callback.Message.Chat.ID, callback.Message.MessageID)
			return true
		}
	}
	return false
}

// StateHandler checkes current state for ChatID we recieved update from
// If returns true : сontinue in main update loop
func (tb *TelegramBot) StateHandler(update tgbotapi.Update) bool {
	if message := update.Message; message != nil {
		state := tb.stateManager.GetState(message.Chat.ID)

		if message.IsCommand() {
			if message.Text == "/create" {
				msg := tgbotapi.NewMessage(message.Chat.ID, "")
				if _, ok := tb.GetStateParams(state, AddSongToPlaylistState); ok {
					msg.Text = "Готово"
					tb.bot.Send(msg)
					tb.MainState(message.Chat.ID)
					return true
				}

				msg.Text = "Неверная команда"
				tb.bot.Send(msg)
				tb.stateManager.SetState(message.Chat.ID, MainState)
				return true
			}
		}
		if state == MainState {
			tb.MainState(message.Chat.ID)
			return true
		}

		// Creating new playlist
		if state == SetPlaylistTitleState {
			id := tb.playlistManager.CreatePlaylist(message.Text, message.Chat.ID)
			tb.stateManager.SetState(message.Chat.ID, AddSongToPlaylistState+":"+types.String(id))

			msg := tgbotapi.NewMessage(message.Chat.ID, "Теперь загрузи песни в свой новый плейлист, когда закончишь, напиши /create.")
			tb.bot.Send(msg)

			return true
		}

		// Adding song to playlist
		if value, ok := tb.GetStateParams(tb.stateManager.GetState(message.Chat.ID), AddSongToPlaylistState); ok {
			// Some messages with audio have Audio field, some Document, implementation for both
			if message.Audio != nil {
				go tb.playlistManager.AddSongToPlaylist(types.Uint64(value), message.Audio.FileID)
				msg := tgbotapi.NewMessage(message.Chat.ID, "Песня успешно добавленна")
				go tb.bot.Send(msg)
				return true
			}
			if message.Document != nil && (message.Document.MimeType == "audio/mpeg" || message.Document.MimeType == "audio/mp3") {
				go tb.playlistManager.AddSongToPlaylist(types.Uint64(value), message.Document.FileID)
				msg := tgbotapi.NewMessage(message.Chat.ID, "Песня успешно добавленна")
				go tb.bot.Send(msg)
				return true
			}
		}

		// If no available variants satisfies the update, set state to main and send control panel
		tb.MainState(message.Chat.ID)
	}

	if callback := update.CallbackQuery; callback != nil {
		state := tb.stateManager.GetState(callback.Message.Chat.ID)
		msg := tgbotapi.NewEditMessageText(
			callback.Message.Chat.ID,
			callback.Message.MessageID,
			"")

		// Download song state
		if state == DownloadState {
			msg.ParseMode = "markdown"
			msg.Text = "*Ты можешь скачать музыку прямо с YouTube" + "\n" + "Просто скинь ссылку на видео, а я все сделаю сам*"
			_, err := tb.bot.Send(msg)
			if err != nil {
				fmt.Println(err.Error())
			}
		}

		// Remove playlist confirm
		if value, ok := tb.GetStateParams(tb.stateManager.GetState(callback.Message.Chat.ID), RemovePlaylistState); ok {
			tb.SendRemoveConfirmationMessage(
				callback.Message.Chat.ID,
				callback.Message.MessageID,
				types.Uint64(value.(string)),
			)
			return true
		}

		// Show playlist songs
		if value, ok := tb.GetStateParams(callback.Data, SelectPlaylistButton); ok {
			switch value.(type) {
			case string:
				tb.PlaylistSongsState(callback.Message.Chat.ID, callback.Message.MessageID, types.Uint64(value))
				tb.SendMainPanelMessage(callback.Message.Chat.ID)
				return true
			}
		}
	}
	return false
}

// Helper methods
func (tb *TelegramBot) SendMainPanelMessage(chatID int64) {
	msg := tgbotapi.NewMessage(chatID, "Панель управления")
	msg.ReplyMarkup = MainPanelButtons
	go tb.bot.Send(msg)
}

func (tb *TelegramBot) SendMainPanelEditMessage(chatID int64, messageID int) {
	msg := tgbotapi.NewEditMessageText(
		chatID,
		messageID,
		"Панель управления")
	msg.ReplyMarkup = &MainPanelButtons

	go tb.bot.Send(msg)
}

func (tb *TelegramBot) SendRemoveConfirmationMessage(chatID int64, messageID int, playlistID uint64) {
	playlist := tb.playlistManager.GetPlaylist(playlistID)
	msg := tgbotapi.NewEditMessageText(
		chatID,
		messageID,
		"")
	msg.ParseMode = "markdown"
	msg.Text = fmt.Sprintf("*Вы уверены что хотите удалить плейлист \"%s\"*\n", playlist.Name)
	msg.ReplyMarkup = RemovePlaylistButtons(playlistID)

	tb.bot.Send(msg)
}

func (tb *TelegramBot) GetStateParams(message string, state string) (interface{}, bool) {
	if strings.Contains(message, state) {
		split := strings.Split(message, ":")
		if len(split) == 3 {
			if split[len(split)-2] == "remove" {
				return obj{"action": "remove", "playlist": split[len(split)-1]}, true
			}
			// add song to playlist selection
			if split[len(split)-2] == "add" {
				return obj{"action": "add", "playlist": split[len(split)-1]}, true
			}
			return obj{"playlist": split[len(split)-2], "song": split[len(split)-1]}, true
		}
		if len(split) == 4 {
			// add downloaded song
			if split[len(split)-3] == "add" {
				return obj{"action": "add", "fileID": split[len(split)-2], "playlist": split[len(split)-1]}, true
			}
		}
		return split[len(split)-1], true
	}
	return nil, false
}

func (tb *TelegramBot) MainState(chatID int64) {
	tb.stateManager.SetState(chatID, MainState)
	go tb.SendMainPanelMessage(chatID)
}

func (tb *TelegramBot) PlaylistSongsState(chatID int64, messageID int, playlistID uint64) {
	songIds := tb.playlistManager.GetAllSongsIDs(playlistID)
	playlist := tb.playlistManager.GetPlaylist(playlistID)

	editMsg := tgbotapi.NewEditMessageText(chatID, messageID, "")
	editMsg.ParseMode = "markdown"
	editMsg.Text = fmt.Sprintf("-\n-\n-\n*Все треки из плейлиста \"%s\"*\n-\n-\n-", playlist.Name)

	for _, id := range songIds {
		audioMsg := tgbotapi.NewAudioShare(chatID, id)
		tb.bot.Send(audioMsg)
	}
	tb.bot.Send(editMsg)
}

func (tb *TelegramBot) downloadVideo(msg *tgbotapi.Message) (string, bool) {
	stringURL := msg.Text
	// Add http:// or https://
	if !(strings.HasPrefix(stringURL, "http://") || strings.HasPrefix(stringURL, "https://")) {
		stringURL = "https://" + stringURL
	}

	u, err := url.ParseRequestURI(stringURL)
	if err != nil {
		tb.bot.Send(tgbotapi.NewMessage(msg.Chat.ID, "Ошибка: некорректная ссылка"))
		return "", false
	}

	v, err := NewVideo(u)
	if err != nil && err.Error() == "Video is longer than 30 minutes" {
		tb.bot.Send(tgbotapi.NewMessage(msg.Chat.ID, "Ошибка: длинна видео больше 30 минут"))
		return "", false
	} else if err != nil {
		tb.bot.Send(tgbotapi.NewMessage(msg.Chat.ID, "Ошибка: некорректная ссылка"))
		return "", false
	}

	msgText := fmt.Sprintf("Видео: %s\n", v.Title)
	botMsg, err := tb.bot.Send(tgbotapi.NewMessage(msg.Chat.ID, msgText))
	if err != nil {
		fmt.Errorf("can't send a message: %s", err)
		return "", false
	}
	msgID := botMsg.MessageID

	for st := range v.Download() {
		var status string
		switch st.(type) {
		case error:
			status = "❌ " + st.(error).Error()
		case string:
			status = st.(string)
		}

		msgText += "\n " + status
		tb.bot.Send(tgbotapi.NewEditMessageText(msg.Chat.ID, msgID, msgText))
	}

	defer os.Remove(v.LinkToAudio)

	sendMsg := tgbotapi.NewAudioUpload(msg.Chat.ID, v.LinkToAudio)
	audioMsg, err := tb.bot.Send(sendMsg)
	if err != nil {
		return "", false
	}
	return audioMsg.Audio.FileID, true
}
