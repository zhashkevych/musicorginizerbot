package main

const (
	DownloadFolder = "./downloads"

	// Database variables
	PlaylistCollection = "playlists"
	SongsCollection    = "songs"

	// Markup variables
	AllPlaylistsButton          = "allPlaylistsBtn"
	AddSongToPlaylistButton     = "addSongToPlaylistBtn"
	RemovePlaylistButton        = "removePlaylistBtn"
	CreatePlaylistButton        = "createPlaylistBtn"
	ReturnButton                = "returnBtn"
	RemovePlaylistConfirmButton = "removePlaylistConfirmBtn"
	RemovePlaylistCancelButton  = "removePlaylistCancelButton"
	SelectPlaylistButton        = "selectPlaylistBtn"
	DownloadButton              = "downloadBtn"

	// State variables
	MainState              = "main"
	ShowAllPlaylistsState  = "allPlaylistsState"
	AddSongToPlaylistState = "addSongToPlaylistState"
	RemovePlaylistState    = "removePlaylistState"
	CreatePlaylistState    = "cretePlaylistState"
	ActivePlaylistState    = "activePlaylistState"
	SetPlaylistTitleState  = "setPlaylistTitleState"
	DownloadState          = "downloadState"
)
