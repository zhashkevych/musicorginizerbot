package main

import (
	"fmt"

	"gopkg.in/night-codes/types.v1"

	"github.com/Syfaro/telegram-bot-api"
)

var (
	MainPanelButtons = tgbotapi.NewInlineKeyboardMarkup(
		tgbotapi.NewInlineKeyboardRow(
			tgbotapi.NewInlineKeyboardButtonData("Все плейлисты", AllPlaylistsButton),
		),
		tgbotapi.NewInlineKeyboardRow(
			tgbotapi.NewInlineKeyboardButtonData("Создать плейлист", CreatePlaylistButton),
		),
		tgbotapi.NewInlineKeyboardRow(
			tgbotapi.NewInlineKeyboardButtonData("Добавить в плейлист", AddSongToPlaylistButton),
			tgbotapi.NewInlineKeyboardButtonData("Удалить плейлист", RemovePlaylistButton),
		),
		tgbotapi.NewInlineKeyboardRow(
			tgbotapi.NewInlineKeyboardButtonData("Скачать песню с YouTube", DownloadButton),
		),
	)
)

// Keyboard for choosing one of the playlist
func AllPlaylistsKeyboard(playlists []*Playlist, action string) tgbotapi.InlineKeyboardMarkup {
	keyboard := make([][]tgbotapi.InlineKeyboardButton, len(playlists)+1)

	for index, playlist := range playlists {
		btnText := types.String(index+1) + ". " + playlist.Name
		btnData := fmt.Sprintf("%s:%d", SelectPlaylistButton, playlist.ID)
		if action != "" {
			btnData = fmt.Sprintf("%s:%s:%d", SelectPlaylistButton, action, playlist.ID)
		}
		btn := tgbotapi.NewInlineKeyboardButtonData(btnText, btnData)

		row := []tgbotapi.InlineKeyboardButton{}
		row = append(row, btn)

		keyboard[index] = append(keyboard[index], row...)
	}

	// "Back" button
	btn := tgbotapi.NewInlineKeyboardButtonData("Назад", ReturnButton)

	row := []tgbotapi.InlineKeyboardButton{}
	row = append(row, btn)

	keyboard[len(playlists)] = append(keyboard[len(playlists)], row...)

	return tgbotapi.InlineKeyboardMarkup{
		InlineKeyboard: keyboard,
	}
}

// Keyboard for delete playlist confirmation
func RemovePlaylistButtons(playlistId uint64) *tgbotapi.InlineKeyboardMarkup {
	markup := tgbotapi.NewInlineKeyboardMarkup(
		tgbotapi.NewInlineKeyboardRow(
			tgbotapi.NewInlineKeyboardButtonData("Удалить",
				fmt.Sprintf("%s:%d", RemovePlaylistConfirmButton, playlistId)),
			tgbotapi.NewInlineKeyboardButtonData("Отмена", RemovePlaylistCancelButton),
		),
	)
	return &markup
}
