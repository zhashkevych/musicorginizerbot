package main

import (
	"fmt"
	"sync"

	ai "github.com/night-codes/mgo-ai"
	mgo "gopkg.in/mgo.v2"
)

type PlaylistManager struct {
	cache      map[uint64]*Playlist
	collection *mgo.Collection
	sync.RWMutex
}

type Playlist struct {
	ID       uint64   `bson:"_id" json:"id"`
	Name     string   `bson:"name" json:"name"`
	ChatID   int64    `bson:"chatId" json:"chatId"`
	SongsIDs []string `bson:"songIds" json:"songIds"`
}

func NewPlaylist(name string, chatId int64) *Playlist {
	ai.Connect(db.C(PlaylistCollection))
	return &Playlist{
		ID:     ai.Next(PlaylistCollection),
		Name:   name,
		ChatID: chatId,
	}
}

func NewPlaylistManager() *PlaylistManager {
	return &PlaylistManager{
		cache:      map[uint64]*Playlist{},
		collection: db.C(PlaylistCollection),
	}
}

func (pm *PlaylistManager) CreatePlaylist(name string, chatId int64) uint64 {
	playlist := NewPlaylist(name, chatId)
	if err := pm.collection.Insert(&playlist); err != nil {
		fmt.Printf("[playlist.go] Error inserting new playlist to DB, err: %s\n", err.Error())
		return playlist.ID
	}
	go pm.UpdateCache()
	return playlist.ID
}

func (pm *PlaylistManager) AddSongToPlaylist(playlistId uint64, songId string) {
	if err := pm.collection.Update(obj{"_id": playlistId}, obj{"$addToSet": obj{"songIds": songId}}); err != nil {
		fmt.Printf("[playlist.go] Error adding song to the playlist, err: %s\n", err.Error())
		return
	}
	go pm.UpdateCache()
}

func (pm *PlaylistManager) RemoveSongFromPlaylist(playlistId uint64, songId string) {
	if err := pm.collection.Update(obj{"_id": playlistId}, obj{"$pull": obj{"songIds": songId}}); err != nil {
		fmt.Printf("[playlist.go] Error adding song to the playlist, err: %s\n", err.Error())
		return
	}
	go pm.UpdateCache()
}

func (pm *PlaylistManager) RemovePlaylist(playlistId uint64) {
	if err := pm.collection.Remove(obj{"_id": playlistId}); err != nil {
		fmt.Printf("[playlist.go] Error removing playlist, err: %s\n", err.Error())
		return
	}
	go pm.UpdateCache()
}

func (pm *PlaylistManager) GetPlaylistByID(playlistId uint64) *Playlist {
	pm.RLock()
	defer pm.RUnlock()
	if playlist, exists := pm.cache[playlistId]; exists {
		return playlist
	}
	return &Playlist{}
}

func (pm *PlaylistManager) GetPlaylist(playlistId uint64) *Playlist {
	pm.RLock()
	defer pm.RUnlock()
	for _, playlist := range pm.cache {
		if playlist.ID == playlistId {
			return playlist
		}
	}
	return &Playlist{}
}

func (pm *PlaylistManager) GetAllPlaylistsFromDB(chatId int64) []*Playlist {
	playlists := []*Playlist{}
	pm.collection.Find(obj{"chatId": chatId}).All(&playlists)
	return playlists
}

func (pm *PlaylistManager) GetAllPlaylists(chatId int64) []*Playlist {
	playlists := []*Playlist{}
	pm.RLock()
	defer pm.RUnlock()
	for _, playlist := range pm.cache {
		if playlist.ChatID == chatId {
			playlists = append(playlists, playlist)
		}
	}
	return playlists
}

func (pm *PlaylistManager) GetAllSongsIDs(playlistId uint64) []string {
	pm.RLock()
	defer pm.RUnlock()
	if playlist, exists := pm.cache[playlistId]; exists {
		return playlist.SongsIDs
	}

	return []string{}
}

func (pm *PlaylistManager) UpdateCache() {
	playlists := []*Playlist{}
	if err := pm.collection.Find(nil).All(&playlists); err != nil {
		fmt.Printf("[playlist.go] Error getting all the playlists from DB, err: %s\n", err.Error())
		return
	}
	pm.Lock()
	for _, playlist := range playlists {
		pm.cache[playlist.ID] = playlist
	}
	pm.Unlock()
}
