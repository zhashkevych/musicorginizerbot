package main

import (
	"net/url"
	"os"
	"path/filepath"
	"time"

	"github.com/pkg/errors"
	"github.com/rylio/ytdl"
)

type Video struct {
	videInfo    *ytdl.VideoInfo
	Title       string
	LinkToAudio string
}

func NewVideo(u *url.URL) (v Video, err error) {
	v = Video{}
	v.videInfo, err = ytdl.GetVideoInfo(u)
	if err != nil {
		return Video{}, err
	}
	if v.videInfo.Duration > (time.Minute * 30) {
		return Video{}, errors.New("Video is longer than 30 minutes")
	}
	v.Title = v.videInfo.Title
	v.LinkToAudio = DownloadFolder + "/" + v.videInfo.Title + ".mp3"

	return v, nil
}

func (v Video) Download() <-chan interface{} {
	responses := make(chan interface{}, 15)

	go func() {
		format, err := func() (form ytdl.Format, err error) {
			err = errors.New("there's no good format")
			for _, f := range v.videInfo.Formats {
				if f.Resolution == "" && f.Extension == "mp4" {
					// Can use any
					if form.AudioBitrate == 0 {
						form = f
						err = nil
						continue
					}
					// Choose the best AudioBitrate
					if f.AudioBitrate > form.AudioBitrate {
						form = f
					}
				}
			}
			return
		}()
		if err != nil {
			responses <- err
			close(responses)
			return
		}

		if _, err := os.Open(v.LinkToAudio); err == nil {
			responses <- errors.New("Video already exists")
			close(responses)
			return
		}

		if _, err := os.Stat(DownloadFolder); os.IsNotExist(err) {
			os.Mkdir(DownloadFolder, os.ModePerm)
		}

		f, err := os.Create(filepath.Join(DownloadFolder, filepath.Base(v.Title+".mp3")))
		if err != nil {
			responses <- err
			close(responses)
			return
		}
		defer f.Close()

		responses <- "💫 Загрузка..."

		err = v.videInfo.Download(format, f)
		if err != nil {
			// Delete bad file
			os.Remove(v.LinkToAudio)
			responses <- err
			close(responses)
			return
		}

		responses <- "🔥 Загрузка завершена"
		close(responses)
	}()

	return responses
}
